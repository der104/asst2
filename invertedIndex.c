#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "tokenizer.h"

static char filename[64];
static int size = 0;
node *used[1000];


int main(int argc, char ** argv)
{
	//////////////////ERROR CHECKS
	char prompt;
	char *path = (char*)malloc(sizeof(argv[2]));
	strcpy(path, argv[2]);
	FILE *fp;

	if( access( path, F_OK ) == -1 )
		{
			fprintf(stderr, "The path %s does not exist. Process terminated", path);
			exit(0);
		}

	 //Simple check to see if there are any CMD line arguments. If not, exit program w/ error msg.
	  if(argc <= 1) //No input
	  {
		  printf("There are no arguments");
		  return 0;
	  }

	  //Check for too many parameters
	  if(argc > 3)
	  {
		  printf("There are too many arguments");
		  return 0;
	  }

	  sprintf(filename, "%s.xml", argv[1]);
	  //if file exists
	  if( (access(filename, F_OK ) != -1) )
	  {
		  while(!(prompt == 'Y')  || !(prompt == 'N'))
		  {
			  printf("File already exists. Would you like to overwrite %s? Y/N\n", filename);
		  	  fflush(stdout);
		  	  scanf("%c", &prompt);
		  	  fflush(stdin);
		      if (prompt == 'Y')
		  	  {
		  		  //file does not exist
		  		  printf("Overwriting XML file...\n");
		  		  fp = fopen(filename, "w");
				  //First line of XML file
  				  fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>",fp);
  				  fputs("\n<fileIndex>",fp);
		  		  break;
		  	  }
		  	  else if (prompt == 'N')
		  	  {
		  		  int cpy = 2;
		  		  char *temp = (char*)malloc(256 * sizeof(char));
		  		  sprintf(temp, "%s(%d).xml",argv[1], cpy);
		  		  while( (access(temp, F_OK ) != -1) )
		  		  {
		  			  cpy = cpy + 1;
			  		  sprintf(temp, "%s(%d).xml",argv[1], cpy);
		  		  }
		  		  printf("Creating XML file, %s\n", temp );

		  		  fp = fopen(temp, "w");
		  		  free(temp);
		  		  fputs("\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>",fp);
		  		  fputs("\n<fileIndex>",fp);
		  		  break;
		  	  }
		  	  else
		  	  {
			  	  printf("Invalid response\n");
		  	  }
		  }
	  }
	  else
	  {
		  //file does not exist
		  printf("XML does not exist. Creating XML file %s\n", filename);
	  	  fflush(stdout);

		  fp = fopen(filename, "w");
		  //First line of XML file
		  fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>",fp);
		  fputs("\n<fileIndex>",fp);
	  }
	  /////////////////ERROR CHECKS
     getTokens(path); //tokenize file/folder
     free(path);//free path
     generateXML(getRoot(), fp, NULL);
     freeNodes(getRoot());
     fputs("\n</fileIndex>", fp);
     fclose(fp);
     printf("Done.");
}

void generateXML(node *root, FILE *fp, node *prev)
{
	char temp_line[256];
	//char temp_filename[256];

	if (root == NULL)
	{
		return;
	}
	if (prev == NULL)
	{
		sprintf(temp_line, "\n\t<word text=\"%s\">", root->string);
		fputs(temp_line, fp);
	}
	else
	{
		if(strcmp(prev->string, root->string) != 0)
		{
			sprintf(temp_line, "\n\t<word text=\"%s\">", root->string);
			fputs(temp_line, fp);
		}
	}
	if(inArray(root, used, size) == 1)
	{//if not in array, print
		sprintf(temp_line, "\n\t\t<file name=\"%s\">%d</file>", root->filename, root->count);			
                fputs(temp_line, fp);
		used[size] = root; //add element to used
		size = size +1;
	}

	if (root->next == NULL)
	{
		fputs("\n\t</word>", fp);
	}
	else
	{
		if(strcmp(root->next->string, root->string) != 0)
		{
			fputs("\n\t</word>", fp);
		}
	}
	generateXML(root->next, fp, root);

}

int getTokens(char *path)
{
	FILE *fp2;
	if(isDirectory(path))
	{
		struct dirent *de;  // Pointer for directory entry
		DIR *dr = opendir(path);
		if (dr == NULL)
		{
		}
		while ((de = readdir(dr)) != NULL)
		{
		    if(de->d_type == DT_REG)
		    {
		    	char temp[256];
		    	char buffer[255];
		    	strcpy(temp, path);
		    	strcat(temp, "/");
		    	strcat(temp, de->d_name);

		    	fp2 = fopen(temp, "r");
		    	while(fgets(buffer, 255, (FILE*) fp2))
		    	{
		    		tokenizer(buffer, de->d_name);
		    	}
			}
		    else if (de->d_type == DT_DIR && (strcmp(de->d_name, "..") !=0 && strcmp(de->d_name, ".") !=0))
		    {
		    	char temp[256];
		    	strcpy(temp, path);
		    	strcat(temp, "/");
		    	strcat(temp, de->d_name);
		    	getTokens(temp);
		    }
		}
		closedir(dr);
		fclose(fp2);
	}
	else
	{
    	fp2 = fopen(path, "r");
    	char buffer[255];

    	while(fgets(buffer, 255, (FILE*) fp2))
    	{
    		tokenizer(buffer, path);
    	}


	}
	return 0;
}

int isDirectory(const char *path)
{
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
   {
	   //printf("this is a directory\n");
       return 0;
   }
   else
	   {
	   //printf("this is a file\n");
	   return S_ISDIR(statbuf.st_mode);
	   }
}

int inArray(node *val, node *arr[], int size){
    int i;
    for(i = 0; i < size; i++)
    {
    	if ((strcmp(arr[i]->string , val->string) == 0) && (strcmp(arr[i]->filename , val->filename) == 0))
    			return  0;
    }
    return 1;
}

