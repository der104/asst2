#ifndef TOKENIZER_H_
#define TOKENIZER_H_

typedef struct treeNode
{
  char* filename; //stores the filename
  char* string; //Stores the char array AKA string read from CMD
  int count;
  struct treeNode* prev;
  struct treeNode* next;
}node;

void freeNodes(node* treeNode);
node* insertNode(node * r, node * n);
int tokenizer(char *argv, char *path);
node *getRoot();
void getCount(node *r);
int getLinkedCount();
int compareNode(node * n1, node * n2);
int isDirectory(const char *path);
int getTokens(char *path);
void generateXML(node *root, FILE *fp, node *prev);
void addToXML(node *tempRoot, FILE *fp);
int inArray(node *val, node* arr[], int size);

#endif /* TOKENIZER_H_ */
