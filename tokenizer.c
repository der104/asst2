#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "tokenizer.h"

//This is the basic node that will be used to create the TREE
node* root;//This is the global root node;
int count = 0;

//This method is to create and return new node
node* createNode(char* string, char* filename)
{
    node* newNode = (node*)malloc(sizeof(node));
    if(newNode == NULL)
    {
      printf("error w/ malloc in Node Creation");
      return 0;
    }
     //newNode->string = string;
     newNode->string =  strdup(string);
     newNode->filename = strdup(filename);
     newNode->prev= NULL;
     newNode->count = 1;
     newNode->next = NULL;
     return(newNode);
}

int tokenizer(char *argv, char * path)
{
  //Here will be the variables used to seperate the chars into indivdual char arrays and counters.

  int inputIndex, skipChars, lengthOfString, oldIndex = 1;

  inputIndex = skipChars = lengthOfString = oldIndex = 0;

  //This is to loop until there is nothing else to read
	while(strlen(argv) > inputIndex)
	{
    //This is to get the count of how many valid chars are in the string
		while(((argv[inputIndex] >='a' && argv[inputIndex] <='z') ||
          (argv[inputIndex] >='A' && argv[inputIndex] <='Z') ||
		  ((argv[inputIndex] >= '0' && argv[inputIndex] <= '9')))	&&
           argv[inputIndex] != '\0' )
		{
        inputIndex++;
		}

		//Check to see that we actually have a string. Mainly if the first chars aren't valid chars
		if(inputIndex != oldIndex) //here, input is
		{
			lengthOfString = inputIndex - oldIndex;//inputIndex +1 to make room for the inserted NULL terminator D NOTE: Changed due to comment above
			char* string = (char*)malloc( lengthOfString * sizeof(char));
                       if(string == NULL)
                       {
                          printf("error w/ malloc in String Creation");
                          return 0;
                       }
			int i;
			for(i=0; i <lengthOfString; i++)
			{
				string[i] = tolower(argv[oldIndex+i]);//This is to load the first valid char
			}
			string[i++] = '\0';//Null Terminate so that we can use string functions on it.
			//Creates a node containing the string
			node* newNode = createNode(string, path);

			//Inserts that node into the tree
			root = insertNode(root, newNode);
			count++;
			//free(string);
		}

		inputIndex++;
		oldIndex = inputIndex;
	}
  return 0;
}

node* getRoot()
{
	return root;
}

void printTree(node * root)
{
	 if (root == NULL)
	 {
		return ;
	 }
	 else
	 {
		 printf("%s(%s)(%d)\n", root->string, root->filename, root->count);
		 printTree(root->next);
	 }

}

void freeNodes(node* treeNode)
{
  if(treeNode == NULL)
    return;

  free(treeNode->next);

}

void getCount(node *r)
{
    node* current = root;
    while (current != NULL)
    {
        if ((strcmp(current->string, r->string) == 0) && (strcmp(current->filename, r->filename) == 0))
           r->count = r->count + 1;
        current = current->next;
    }

}

node* insertNode(node *r, node * n)
{
	getCount(n);
	if(r == NULL){
        return n;
	}
	node* temp = r;
	node* prev = NULL;
	while(temp != NULL && compareNode(n, temp) < 0)
    {
		prev = temp;
		temp = temp->next;
    }
	    if(temp == NULL){
	        prev->next = n;
	        n->prev = prev;
	        return r;
	    }
	    if(prev == NULL){
	        r->prev = n;
	        n->next = r;
	        return n;
	    }
	    prev->next = n;
	    n->prev = prev;
	    n->next = temp;
	    temp->prev = n;
	    return r;
}

int getLinkedCount()
{
	return count;
}

int compareNode(node * to_add, node * to_comp)
{
	if (strcmp(to_add->string, to_comp->string) > 0)
	{//zoop aab
		return -1;
	}

	else if (strcmp(to_add->string, to_comp->string) == 0)
	{//aab aab
		if (strcmp(to_add->filename, to_comp->filename) > 0)
		{//aab|zoop aab|aab
			if (to_add->count == to_comp->count)
				return -1;
			if (to_add->count < to_comp->count)
				return -1;
		}
		if (strcmp(to_add->filename, to_comp->filename) == 0)
		{//aab|aab aab|aab
			if (to_add->count < to_comp->count)
				return -1;
	}
		if (strcmp(to_add->filename, to_comp->filename) < 0)
		{//aab|aab aab|zoop
			if (to_add->count < to_comp->count)
				return -1;
		}
	}

	return 1;
}
